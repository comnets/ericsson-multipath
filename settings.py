# Program
SYMBOLS = 50
SYMBOL_SIZE = 1000
# INTERFACE_DST_IP = {INTERFACE_0: "10.0.0.1", INTERFACE_1: "10.0.0.2"}
CLIENT_INF_LST = ["wifi", "LTE"]
# CLIENT_INF_LST = ["local"]
CLIENT_INF = {"wifi": "h1-eth0", "LTE": "h1-eth1", "local": "lo"}
CLIENT_IP = {"wifi": "10.0.0.1", "LTE": "192.168.0.1", "local": "127.0.0.1"}
SERVER_IP = "10.0.0.2"
SERVER_INF = "h2-eth0"
# SERVER_IP = "127.0.0.1"
# SERVER_INF = "lo"

DATA_PORT = 5001
FB_PORT = 5010
FB_CYCLE = 0.00001
SEND_CYCLE = 0.000005
# base64 /dev/urandom | head -c 10000000 > test2.txt
TEST_FILE = "/home/justus/work/ericsson-multipath/test3.txt"
QUEUE_LENGTH = 10
RCV_BUFFSIZE = 1100

# Statistics
STATS_FILE_IPERF = "log6/iperf/"
STATS_FILE_CM = "log6/codedmp/"
STATS_FILE_UCM = "log6/uncodedmp/"

# Simulation parameters
GOOD_BW = 5000
BAD_BW = 1000
GOOD_DUR = ["1.5", "3", "6", "9"]
BAD_DUR = ["0.5", "1", "2", "3"]
#GOOD_DUR = ["9"]
#BAD_DUR = ["3"]

# FEEDBACK structure:
# ACK redundant not needed/useful generations --> [2,3.4] means 2,3,4 are already finished
