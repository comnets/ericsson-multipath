import threading
import queue
import socket
import sys
import struct
import helper_functions as hf
import settings as sett
import time
import random
import os
import json
import argparse
import bisect


class Client:
    def __init__(self, log="/tmp/test.log"):
        self.threads = []

        self.file_size = None
        self.generations = None
        self.packet_dict = {}
        self.finished_packets = []
        self.max_header_no = [0]
        self.exitFlag = [False]
        self.data_queue = queue.Queue(sett.QUEUE_LENGTH)
        self.recv_packets = {}
        self.feedback = []
        self.send_feedback = [False]
        self.sent_feedbacks = [0]

        self.data_decoder_threads = {}
        self.send_feedback_thread = None
        self.send_feedback_threads = {}
        self.status_thread = None

        self.start_time = [0]
        self.end_time = [0]

        # self.stats_file = open(sett.STATS_FILE_CM, "a")
        self.stats_file = open(log, "a")
        # self.stats_file.write("timestamp,total_time,received_packets,decoded_packets,throughput,goodput\n")

    def prepare(self):
        pass

    def start_threads(self):
        self.prepare()

        # TODO: pass self variable instead of each single one???

        for medium in sett.CLIENT_INF_LST:
            self.send_feedback_threads[medium] = SendFeedbackThread(sett.CLIENT_INF[medium], sett.FB_PORT,
                                                                    self.feedback, self.send_feedback,
                                                                    self.sent_feedbacks,
                                                                    self.exitFlag)
            self.send_feedback_threads[medium].start()

            self.recv_packets[medium] = [0]
            self.data_decoder_threads[medium] = DataDecoderThread(sett.CLIENT_INF[medium], self.start_time,
                                                                  self.end_time, self.packet_dict,
                                                                  self.finished_packets, self.data_queue,
                                                                  self.recv_packets[medium],
                                                                  self.feedback, self.send_feedback, self.max_header_no,
                                                                  self.stats_file,
                                                                  self.exitFlag)
            self.data_decoder_threads[medium].start()

        self.threads.extend(self.send_feedback_threads.values())
        self.threads.extend(self.data_decoder_threads.values())

        self.status_thread = StatusThread(self.recv_packets, self.packet_dict, self.finished_packets,
                                          self.max_header_no, self.exitFlag)
        self.status_thread.start()
        self.threads.append(self.status_thread)
        self.check_status()

    def check_status(self):
        while True:
            active_thread_count = 0
            for thread in self.threads:
                if thread.isAlive():
                    active_thread_count += 1
            if active_thread_count == 0:
                break
            time.sleep(0.5)
        self.calc_stats()
        self.stats_file.close()

    def calc_stats(self):
        # TODO: calc throughput and goodput
        total_time = self.end_time[0] - self.start_time[0]
        recv_packets_sum = 0
        for media in self.recv_packets:
            recv_packets_sum += self.recv_packets[media][0]
        data_sum = recv_packets_sum * (8 + sett.SYMBOL_SIZE)
        # Dont know why coded packets is one byte larger than SYMBOL_SIZE + log_2(q)*SYMBOLS
        data_eff = self.max_header_no[0] * sett.SYMBOL_SIZE
        decoded_packets = self.max_header_no[0]
        throughput = data_sum / total_time * 8
        goodput = data_eff / total_time * 8
        print("########################### WARNING: Does not consider packets lost due to congestion #################")
        print("Time: {}".format(total_time))
        print("Received pkts: {} Decoded pkts: {}".format(recv_packets_sum, decoded_packets))
        print("Thoughput bits/s: {}".format(throughput))
        print("Goodput bits/s: {}".format(goodput))
        # self.stats_file.write("timestamp,total_time,received_packets,decoded_packets,throughput,goodput\n")
        self.stats_file.write(
            "{},{},{},{},{},{},{}\n".format(time.time(), total_time, recv_packets_sum, decoded_packets, int(throughput),
                                            int(goodput), self.sent_feedbacks[0]))


class DataDecoderThread(threading.Thread):
    def __init__(self, ifname, start_time, end_time, packet_dict, finished_packets, data_queue,
                 recv_packets, feedback, send_feedback,
                 max_header_no, stats_file, exitFlag):
        threading.Thread.__init__(self)
        self.ifname = ifname
        self.data_port = sett.DATA_PORT
        self.start_time = start_time
        self.end_time = end_time
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.bind((hf.get_ip_address(ifname), self.data_port))
        self.sock.settimeout(1)
        self.packet_dict = packet_dict
        self.finished_packets = finished_packets
        self.max_header_no = max_header_no
        self.data_queue = data_queue
        self.recv_packets = recv_packets
        self.feedback = feedback
        self.send_feedback = send_feedback
        self.stats_file = stats_file
        self.exitFlag = exitFlag

    def run(self):
        while not self.exitFlag[0]:
            try:
                data = self.sock.recv(sett.RCV_BUFFSIZE)
            except socket.timeout:
                print("No packet received")
                continue
            # print("Data header: {}".format(data[:8]))
            self.recv_packets[0] = self.recv_packets[0] + 1
            if self.start_time[0] == 0:
                self.start_time[0] = time.time()
            generation = int(struct.unpack('i', data[:4])[0])
            if self.max_header_no[0] == 0:
                self.max_header_no[0] = int(struct.unpack('i', data[4:8])[0])
                print("Max_header_no: {}".format(self.max_header_no[0]))

            # https://stackoverflow.com/questions/7571635/fastest-way-to-check-if-a-value-exist-in-a-list
            # if generation in self.finished_packets:
            index = bisect.bisect_left(self.finished_packets, generation)
            if index < len(self.finished_packets) and self.finished_packets[index] == generation:
                self.feedback.append(generation)
                self.send_feedback[0] = True
            else:
                self.packet_dict[generation] = data[8:]
                self.finished_packets.append(generation)
                self.feedback.append(generation)
                self.send_feedback[0] = True
            if len(self.finished_packets) == self.max_header_no[0] and len(self.finished_packets) > 0:
                self.end_time[0] = time.time()
                self.exitFlag[0] = True
        print("Exit DataDecoderThread")


class SendFeedbackThread(threading.Thread):
    def __init__(self, ifname, fb_port, feedback, send_feedback, sent_feedbacks, exitFlag):
        threading.Thread.__init__(self)
        self.ifname = ifname
        self.fb_port = fb_port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        # self.sock.bind((hf.get_ip_address(ifname), self.fb_port))
        self.exitFlag = exitFlag
        self.feedback = feedback
        self.send_feedback = send_feedback
        self.sent_feedbacks = sent_feedbacks

    def run(self):
        while not self.exitFlag[0]:
            if len(self.feedback) > 0 and self.send_feedback[0]:
                # print("Feedback: {}".format(self.feedback))
                feedback = json.dumps(self.feedback).encode("utf8")
                self.sock.sendto(feedback, (sett.SERVER_IP, self.fb_port))
                self.feedback.clear()
                self.send_feedback[0] = False
                self.sent_feedbacks[0] += 1
            else:
                # FIXME: This is not smart....
                time.sleep(sett.SEND_CYCLE)
        for i in range(3):
            print("Send finished")
            self.sock.sendto("finished".encode("utf8"), (sett.SERVER_IP, self.fb_port))
            time.sleep(0.5)
        print("Exit SendFeedbackThread")


class StatusThread(threading.Thread):
    def __init__(self, recv_packets, packet_dict, finished_packets, max_header_no, exitFlag):
        threading.Thread.__init__(self)
        self.recv_packets = recv_packets
        self.packet_dict = packet_dict
        self.finished_packets = finished_packets
        self.max_header_no = max_header_no
        self.exitFlag = exitFlag
        self.recv_packets_sum = 0

    def run(self):
        while not self.exitFlag[0]:
            self.recv_packets_sum = 0
            for intf in self.recv_packets:
                self.recv_packets_sum += self.recv_packets[intf][0]
            decoded_packets = len(self.packet_dict.keys())
            print(
                "Number of received packets: {} vs. decoded packets: {}".format(self.recv_packets_sum, decoded_packets))
            # print("Decoders rank: {}".format(sorted([decoder.rank() for decoder in self.decoders])))
            print("Progress: {}/{}".format(len(self.finished_packets), self.max_header_no[0]))
            if len(self.finished_packets) == self.max_header_no[0] and len(self.finished_packets) > 0:
                self.exitFlag[0] = True
                break
            time.sleep(1)
        print("Exit StatusThread")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--log", type=str, default="/tmp/test.log",
                        help="Run the tunnel in xterm. This makes the stdout of nctun visible")
    args = parser.parse_args()

    c = Client(log=args.log)
    c.start_threads()
