#!/usr/bin/env python3

import argparse
import random
import sched
import os


# seeds = {"LTE":0, "wifi":1}

class State:
    def __init__(self, transitions, callback, args=(), kwargs={}):
        self.transitions = transitions
        self.callback = callback
        self.args = args
        self.kwargs = kwargs

    def call(self):
        self.callback(*self.args, **self.kwargs)


class StateEngine_expo:
    def __init__(self, states):
        self.states = states
        self.state = None
        self.sched = None
        self.random = None

    def start(self, sched, rand):
        self.sched = sched
        self.rand = rand

        self.toggle(0)

    def toggle(self, state):
        self.state = self.states[state]
        self.state.call()

        trans = self.state.transitions

        next, delay = min([(s, self.rand.expovariate(t)) for s, t in enumerate(trans) if t > 0], key=lambda a: a[1])
        self.sched.enter(delay, 1, self.toggle, [next])

class StateEngine_fix:
    def __init__(self, states):
        self.states = states
        self.state = None
        self.sched = None
        self.random = None

    def start(self, sched, rand):
        self.sched = sched
        self.rand = rand

        self.toggle(0)

    def toggle(self, state):
        self.state = self.states[state]
        self.state.call()

        trans = self.state.transitions

        next, delay = min([(s, 1/t) for s, t in enumerate(trans) if t > 0], key=lambda a: a[1])
        self.sched.enter(delay, 1, self.toggle, [next])


def configure(path, bw, loss, latency, log):
    # print("latency: {}".format(latency))
    # os.system('./configure.sh %s %dkbit %dms %f%%' % (path, bw, latency, loss))
    os.system('./configure.sh %s %d %dms %f%% %s' % (path, bw, latency, loss, log))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--path', default='wifi',
                        help='Name of the path to modulate')
    parser.add_argument('--seed', default='0',
                        help='Specify seed for random numbers')
    parser.add_argument('--log', default='/tmp/mod.log',
                        help='Specify logfile')
    for state in ['good', 'bad']:
        parser.add_argument('--%s-duration' % state, type=float, default=1,
                            help='Mean duration (s) of the %s state' % state)
        parser.add_argument('--%s-bw' % state, type=int, default=10000,
                            help='Bandwidth limitation (kbit/s) for the %s state' % state)
        parser.add_argument('--%s-loss' % state, type=float, default=0,
                            help='Loss probability (%%%%) for the %s state' % state)
        parser.add_argument('--%s-latency' % state, type=int, default=10,
                            help='Latency (ms) for the %s state' % state)
    args = parser.parse_args()

    # os.system('ovs-vsctl set interface {} ingress_policing_burst=100'.format(args.path))
    good_state = State([0, 1 / args.good_duration], configure,
                       [args.path, args.good_bw, args.good_loss, args.good_latency, args.log])
    bad_state = State([1 / args.bad_duration, 0], configure,
                      [args.path, args.bad_bw, args.bad_loss, args.bad_latency, args.log])

    engine = StateEngine_expo([good_state, bad_state])

    s = sched.scheduler()
    r = random.Random()  # TODO: use same seed for same technologies but different for other links
    r.seed(args.seed)
    engine.start(s, r)
    s.run()
