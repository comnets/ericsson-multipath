#!/bin/sh

DEV=$1
BW=$2
Q=$3

ovs-vsctl -- set Port $DEV qos=@newqos -- \
--id=@newqos create QoS type=linux-htb other-config:max-rate=1000000000 queues:$Q=@q$Q -- \
--id=@q$Q create Queue other-config:max-rate=$BW
