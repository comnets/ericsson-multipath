#!/usr/bin/env python2
import sys

sys.path.append("..")
import settings as sett


def create_network(paths, loss=0.0, latency='10ms,10ms'):
    from mininet.net import Mininet
    from mininet.link import TCLink
    from mininet.node import CPULimitedHost, OVSController, OVSSwitch
    from mininet.log import info

    latencys = latency.split(",")
    net = Mininet(controller=OVSController, link=TCLink, switch=OVSSwitch)

    # since we need a switch we also need a controller
    c0 = net.addController('c0')
    s1 = net.addSwitch('s1')
    s2 = net.addSwitch('s2')

    # create virtual hosts
    h1 = net.addHost('h1')
    h2 = net.addHost('h2')

    info("Create link s1 <-> h2 ")
    li = net.addLink(s1, h2)
    info("\n")

    info("Create link s1 <-> s2 ")
    li = net.addLink(s1, s2)
    info("\n")

    info("Create link h1 <-> s2 ")
    li = net.addLink(h1, s2, loss=loss, delay=latencys[0], bw=10, use_tbf=True)
    info("\n")

    # create the links and other policing switches
    s = []
    for i in range(paths - 1):
        info("Create switch s{}".format(i + 3))
        info("\n")
        s.append(net.addSwitch('s{}'.format(i + 3)))
        info("Create link s1 <-> s{}".format(i + 3))
        info("\n")
        li = net.addLink(s1, s[i])
        info("Create link h1 <-> s{}".format(i + 3))
        li = net.addLink(h1, s[i], loss=loss, delay=latencys[i + 1], bw=10, use_tbf=True)
        li.intf1.setIP("192.168.%d.1" % i, 24)
        info("\n")

    net["s2"].cmd("ifconfig s2-eth2 txqueuelen 0")
    net["s3"].cmd("ifconfig s3-eth2 txqueuelen 0")

    return net


def set_route(intf, gateway, table):
    intf.cmd("ip rule add from %s table %d" % (intf.ip, table), shell=True)
    shift = 32 - int(intf.prefixLen)
    netnum = reduce(lambda l, r: (l << 8) + int(r), intf.ip.split("."), 0) >> shift << shift
    netip = ".".join([str((netnum >> (24 - i * 8)) & 0xff) for i in range(4)])
    intf.cmd("ip route add %s/%s dev %s scope link table %d" % (netip, intf.prefixLen, intf.name, table), shell=True)
    intf.cmd("ip route add default dev %s table %d" % (intf.name, table), shell=True)


def sysctl(name, value):
    from subprocess import check_output
    return check_output(["sysctl", "-w", str(name) + "=" + str(value)])


def set_parameter(name, value):
    from subprocess import check_output
    with open(name, "w") as subf:
        subf.write(str(value))
    return name + " = " + check_output(["cat", name])


def setup(net):
    h1 = net["h1"]
    h2 = net["h2"]

    for i in h1.intfs:
        h1.intfs[i].ifconfig("down")
    sleep(1)

    for i in h1.intfs:
        intf1 = h1.intfs[i]

        intf1.ifconfig("up")
        intf1.cmd("ip link set dev %s multipath on", intf1.name)
        set_route(intf1, h2.intfs[0].ip, i + 1)

        if i == 0:
            h1.cmd("ip route add default scope global nexthop via %s dev %s" % (h2.intfs[0].ip, intf1.name), shell=True)

    sys.stderr.write("h1: ip rule show\n" + h1.cmdPrint("ip rule show"))
    sys.stderr.write("h1: ip route\n" + h1.cmdPrint("ip route"))

    for i in h1.intfs:
        sys.stderr.write(("h1: ip route show table %d\n" % (i + 1)) + h1.cmdPrint("ip route show table %d" % (i + 1)))

    h2.cmd("ip route add default dev %s" % h2.defaultIntf().name)

    sys.stderr.write("h2: ip rule show\n" + h2.cmdPrint("ip rule show"))
    sys.stderr.write("h2: ip route\n" + h2.cmdPrint("ip route"))

    for i in h2.intfs:
        sys.stderr.write(("h2: ip route show table %d\n" % (i + 1)) + h2.cmdPrint("ip route show table %d" % (i + 1)))


def start_bwm(node, filename=None):
    from mininet.term import makeTerm

    cmd = ["bwm-ng", "-u", "bits"]
    if filename:
        # return node.popen(cmd + ["-o", "csv", "-F", filename, "-T", "rate"])
        # print("bwm-ng -u bits -o csv -F {}-{}.csv -T rate".format(filename,node.name))
        makeTerm(node, cmd="bwm-ng -u bits -o csv -F {} -T rate".format(filename))
    else:
        makeTerm(node, cmd="bash -c '%s || read'" % " ".join(cmd))
        return None


def start_htop(node):
    from mininet.term import makeTerm

    cmd = ["htop"]
    makeTerm(node, cmd="bash -c '%s || read'" % " ".join(cmd))


def start_tcpdump(node):
    return node.popen(["tcpdump", "-i", "any", "-s", "65535", "-w", node.name + ".pcap"])


def start_iperf(net, sender, receiver, paths, repeat):
    from mininet.term import makeTerm
    import time

    file_size = 100000000
    print("receiver ip: {}".format(receiver.IP()))
    s1 = net['s1']
    for index, value in enumerate(sett.GOOD_DUR):
        log_file = sett.STATS_FILE_IPERF
        start_bwm(s1, log_file + "bwm" + sett.GOOD_DUR[index] + sett.BAD_DUR[index] + ".csv")
        log_file_iperf = log_file + "stats" + sett.GOOD_DUR[index] + sett.BAD_DUR[index] + ".csv"
        try:
            f = open(log_file_iperf, "r")
            old_num_lines = sum(1 for line in f)
        except IOError as e:
            f = open(log_file_iperf, "w")
            f.close()
            old_num_lines = 0
        num_lines = old_num_lines
        makeTerm(receiver, cmd='iperf -s -y C >> {}'.format(log_file_iperf))
        for i in range(repeat):
            for j in range(paths):
                makeTerm(net["s{}".format(j + 2)],
                         cmd='python3 modulator.py --path s{}-eth2 --good-bw {} --bad-bw {} --good-duration {} --bad-duration {} --seed {} --log {}'.format(
                             j + 2, sett.GOOD_BW, sett.BAD_BW, sett.GOOD_DUR[index], sett.BAD_DUR[index], i+j,
                             log_file + "s{}-eth1_".format(j + 2) + sett.GOOD_DUR[index] + sett.BAD_DUR[
                                 index] + ".csv"))
            time.sleep(1)
            # sender_term = makeTerm(receiver, cmd='iperf -c {} -n {}'.format(receiver.IP(),file_size))
            makeTerm(sender, cmd='iperf -c {} -n {} -i 1'.format(receiver.IP(), file_size))

            # Bad workaround to see if server has received the complete transmission
            while 1:
                with open(log_file_iperf) as f:
                    num_lines = sum(1 for line in f)
                if num_lines == old_num_lines:
                    time.sleep(0.5)
                    continue
                elif num_lines == old_num_lines + 1:
                    old_num_lines = num_lines
                    break
                else:
                    time.sleep(2)
                    print("Some error occured (old,new): {},{}".format(old_num_lines, num_lines))
            print("Sender finished")
            for j in range(paths):
                net["s{}".format(j + 2)].cmd("pkill -f modulator.py")
        receiver.cmd("pkill iperf")
        s1.cmd("pkill bwm-ng")



def start_codedmp(net, sender, receiver, paths, repeat):
    from mininet.term import makeTerm
    import time

    s1 = net['s1']
    for index, value in enumerate(sett.GOOD_DUR):
        log_file = sett.STATS_FILE_CM
        start_bwm(s1, log_file + "bwm" + sett.GOOD_DUR[index] + sett.BAD_DUR[index] + ".csv")
        log_file_codedmp = log_file + "stats" + sett.GOOD_DUR[index] + sett.BAD_DUR[index] + ".csv"
        try:
            f = open(log_file_codedmp, "r")
            old_num_lines = sum(1 for line in f)
        except IOError as e:
            f = open(log_file_codedmp, "w")
            f.close()
            old_num_lines = 0
        num_lines = old_num_lines
        print("receiver ip: {}".format(receiver.IP()))

        for i in range(repeat):
            # sender_term = makeTerm(receiver, cmd='iperf -c {} -n {}'.format(receiver.IP(),file_size))
            makeTerm(receiver, cmd="python3 ../coded_mp_receiver.py --log  {}".format(
                log_file_codedmp))
            for j in range(paths):
                makeTerm(net["s{}".format(j + 2)],
                         cmd='python3 modulator.py --path s{}-eth2 --good-bw {} --bad-bw {} --good-duration {} --bad-duration {} --seed {} --log {}'.format(
                             j + 2, sett.GOOD_BW, sett.BAD_BW, sett.GOOD_DUR[index], sett.BAD_DUR[index], i+j,
                             log_file + "s{}-eth1_".format(j + 2) + sett.GOOD_DUR[index] + sett.BAD_DUR[
                                 index] + ".csv"))
            time.sleep(1)
            makeTerm(sender, cmd="python3 ../coded_mp_sender.py")

            # Bad workaround to see if server has received the complete transmission
            while 1:
                with open(log_file_codedmp) as f:
                    num_lines = sum(1 for line in f)
                if num_lines == old_num_lines:
                    time.sleep(0.5)
                    continue
                elif num_lines == old_num_lines + 1:
                    old_num_lines = num_lines
                    break
                else:
                    time.sleep(2)
                    print("Some error occured (old,new): {},{}".format(old_num_lines, num_lines))
            print("Sender finished")
            for j in range(paths):
                net["s{}".format(j + 2)].cmd("pkill -f modulator.py")
            sender.cmd("pkill -f coded_mp_sender.py")
            time.sleep(0.5)
        s1.cmd("pkill bwm-ng")

def start_uncodedmp(net, sender, receiver, paths, repeat):
    from mininet.term import makeTerm
    import time

    s1 = net['s1']
    for index, value in enumerate(sett.GOOD_DUR):
        log_file = sett.STATS_FILE_UCM
        start_bwm(s1, log_file + "bwm" + sett.GOOD_DUR[index] + sett.BAD_DUR[index] + ".csv")
        log_file_uncodedmp = log_file + "stats" + sett.GOOD_DUR[index] + sett.BAD_DUR[index] + ".csv"
        try:
            f = open(log_file_uncodedmp, "r")
            old_num_lines = sum(1 for line in f)
        except IOError as e:
            f = open(log_file_uncodedmp, "w")
            f.close()
            old_num_lines = 0
        num_lines = old_num_lines
        print("receiver ip: {}".format(receiver.IP()))
        for i in range(paths):
            makeTerm(net["s{}".format(i + 2)],
                     cmd='python3 modulator.py --path s{}-eth2 --good-bw {} --bad-bw {} --good-duration {} --bad-duration {} --seed {} --log {}'.format(
                         i + 2, sett.GOOD_BW, sett.BAD_BW, sett.GOOD_DUR[index], sett.BAD_DUR[index], i,
                         log_file + "s{}-eth1_".format(i + 2) + sett.GOOD_DUR[index] + sett.BAD_DUR[index] + ".csv"))
        time.sleep(1)
        for i in range(repeat):
            # sender_term = makeTerm(receiver, cmd='iperf -c {} -n {}'.format(receiver.IP(),file_size))
            makeTerm(receiver, cmd="python3 ../uncoded_mp_receiver.py --log {}".format(
                log_file_uncodedmp))
            for j in range(paths):
                makeTerm(net["s{}".format(j + 2)],
                         cmd='python3 modulator.py --path s{}-eth2 --good-bw {} --bad-bw {} --good-duration {} --bad-duration {} --seed {} --log {}'.format(
                             j + 2, sett.GOOD_BW, sett.BAD_BW, sett.GOOD_DUR[index], sett.BAD_DUR[index], i+j,
                             log_file + "s{}-eth1_".format(j + 2) + sett.GOOD_DUR[index] + sett.BAD_DUR[
                                 index] + ".csv"))
            time.sleep(1)
            makeTerm(sender, cmd="python3 ../uncoded_mp_sender.py")

            # Bad workaround to see if server has received the complete transmission
            while 1:
                with open(log_file_uncodedmp) as f:
                    num_lines = sum(1 for line in f)
                if num_lines == old_num_lines:
                    time.sleep(0.5)
                    continue
                elif num_lines == old_num_lines + 1:
                    old_num_lines = num_lines
                    break
                else:
                    time.sleep(2)
                    print("Some error occured (old,new): {},{}".format(old_num_lines, num_lines))
            print("Sender finished")
            sender.cmd("pkill -f uncoded_mp_sender.py")
            for j in range(paths):
                net["s{}".format(j + 2)].cmd("pkill -f modulator.py")
            time.sleep(0.5)
        s1.cmd("pkill bwm-ng")


def pingall(*nodes):
    for s in nodes:
        for d in nodes:
            if s == d:
                continue
            for i in s.intfs:
                for j in d.intfs:
                    sys.stderr.write(s.cmdPrint(["ping", "-I", s.intfs[i].name, "-c", "4", d.intfs[j].ip]))


if __name__ == '__main__':
    from time import sleep
    import sys
    import argparse

    parser = argparse.ArgumentParser("Setup a Multipath environment and run a benchmark")
    parser.add_argument("--term", action='store_true',
                        help="Run the tunnel in xterm. This makes the stdout of nctun visible")
    parser.add_argument("--cli", action='store_true',
                        help="Run the mininet CLI instead of the benchmark")
    parser.add_argument("--xterms", action='store_true',
                        help="Start xterms for each node")
    parser.add_argument("--bwm", default=None,
                        help="Run a bandwidth monitor on the switch and save to a csv file")
    parser.add_argument("--bwm-term", action='store_true',
                        help="Run a bandwidth monitor on the nodes")
    parser.add_argument("--htop", action='store_true',
                        help="Run a htop on the nodes")
    parser.add_argument("--iperf", action='store_true',
                        help="Run an iperf measurement on the nodes")
    parser.add_argument("--codedmp", action='store_true',
                        help="Run a coded multipath measurement on the nodes")
    parser.add_argument("--uncodedmp", action='store_true',
                        help="Run a coded multipath measurement on the nodes")
    parser.add_argument("--tcpdump", action='store_true',
                        help="Use tcpdump to store the transfered packets")
    parser.add_argument("--log", default=None,
                        help="Set the mininet log level")
    parser.add_argument("--bw", default=10, type=float,
                        help="Bandwidth in Mbps for each path")
    parser.add_argument("--paths", default=2, type=int,
                        help="Maximum number of paths between the nodes")
    parser.add_argument("--loss", default=0, type=int,
                        help="Loss percentage for each link")
    parser.add_argument("--latency", default="10ms,10ms",
                        help="Latency of a single packet transmission")
    parser.add_argument("--time", type=int, default=10,
                        help="Duration of the benchmark")
    parser.add_argument("--repeat", type=int, default=1,
                        help="Number of times to repeat one measurement")
    parser.add_argument("--mptcp-disabled", action='store_true',
                        help="Disable the kernel mptcp support")
    parser.add_argument("--mptcp-syn-retries", type=int, default=3,
                        help="""Specifies how often we retransmit a SYN with the
            MP_CAPABLE-option. After this, the SYN will not contain the
            MP_CAPABLE-option. This is to handle middleboxes that drop SYNs
            with unknown TCP options.""")
    parser.add_argument("--mptcp-no-checksum", action='store_true', default=False,
                        help="Disable the MPTCP checksum")
    parser.add_argument("--mptcp-path-manager", default="fullmesh",
                        help="Select the MPTCP path manager")
    parser.add_argument("--mptcp-subflows", default=1,
                        help="Number of subflows to use")
    parser.add_argument("--congestion-control", default="olia",
                        help="Congestion control algorithm")
    parser.add_argument("--topo-only", action='store_true',
                        help="Just create the topology for other tests")

    args = parser.parse_args()

    # set the log level to get some feedback from mininet
    if args.log:
        from mininet.log import setLogLevel

        setLogLevel(args.log)

    if not args.topo_only:
        if args.mptcp_disabled:
            sys.stderr.write(sysctl("net.mptcp.mptcp_enabled", 0))
        else:
            sys.stderr.write(sysctl("net.mptcp.mptcp_enabled", 1))
            sys.stderr.write(sysctl("net.mptcp.mptcp_syn_retries", args.mptcp_syn_retries))
            if args.mptcp_no_checksum:
                sys.stderr.write(sysctl("net.mptcp.mptcp_checksum", 0))
            else:
                sys.stderr.write(sysctl("net.mptcp.mptcp_checksum", 1))
            sys.stderr.write(sysctl("net.mptcp.mptcp_path_manager", args.mptcp_path_manager))
            sys.stderr.write(sysctl("net.ipv4.tcp_congestion_control", args.congestion_control))

            sys.stderr.write(set_parameter("/sys/module/mptcp_%s/parameters/num_subflows" % args.mptcp_path_manager,
                                           args.mptcp_subflows))

    net = create_network(paths=args.paths, loss=args.loss, latency=args.latency)
    net.start()

    s1 = net['s1']
    s2 = net['s2']
    s3 = net['s3']
    h1 = net['h1']
    h2 = net['h2']

    sleep(1)
    setup(net)
    pingall(h1, h2)

    procs = []

    if args.bwm:
        procs.append(start_bwm(s1, args.bwm))

    if args.iperf:
        procs.append(start_iperf(net, h2, h1, args.paths, args.repeat))

    if args.codedmp:
        procs.append(start_codedmp(net, h2, h1, args.paths, args.repeat))

    if args.uncodedmp:
        procs.append(start_uncodedmp(net, h2, h1, args.paths, args.repeat))

    if args.bwm_term:
        start_bwm(h1)
        start_bwm(h2)

    if args.htop:
        start_htop(h1)
        start_htop(h2)

    if args.tcpdump:
        procs.append(start_tcpdump(h1))
        procs.append(start_tcpdump(h2))

    sleep(2)
    if args.xterms:
        net.startTerms()
    if args.cli:
        from mininet.cli import CLI

        CLI(net)
    else:
        pass
        # for _ in range(args.repeat):
        #    sleep(1)
        #    result = net.iperf(seconds=args.time, fmt="m")
        #    print
        #    "%s %s" % (result[0], result[1])

    for p in procs:
        if p:
            p.terminate()
            p.wait()

    net.stop()
