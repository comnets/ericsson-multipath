#!/bin/bash

DEV=$1
BW=$2
LAT=$3
LOSS=$4
LOG=$5
# BURST=$(echo "$BW * 0.8/1" | bc)
#ovs-vsctl set interface $DEV ingress_policing_rate=$BW
#ovs-vsctl set interface $DEV ingress_policing_burst=100 #$BURST
#echo "ovs-vsctl set interface $DEV ingress_policing_rate=$BW"
#ovs-vsctl -- set Port $DEV qos=@newqos -- \
#--id=@newqos create QoS type=linux-htb other-config:max-rate=1000000000 queues=0=@q0 -- \
#--id=@q0 create Queue other-config:max-rate=$BW

tc qdisc change dev $DEV root tbf rate ${BW}Kbit burst 15000b latency 12.0ms || tc qdisc add dev $DEV root tbf rate ${BW}Kbit burst 15000b latency 12.0ms
echo "tc qdisc change dev $DEV root tbf rate ${BW}Kbit burst 15000b latency 12.0ms"
#tc qdisc change dev $DEV parent 5:1 netem delay $LAT loss random $LOSS || tc qdisc add dev $DEV root netem delay $LAT loss random $LOSS
#echo "tc qdisc change dev $DEV root netem delay $LAT loss random $LOSS"
#tc class change dev $DEV parent 5:0 classid 5:1 htb rate $BW
#echo "tc class change dev $DEV parent 5:0 classid 5:1 htb rate $BW"
echo "$(date +'%s%3N'),$DEV,$BW,$LAT,$LOSS" >> $LOG
