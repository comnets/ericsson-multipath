#! /usr/bin/python3
import sys
import numpy as np
import statsmodels.stats.api as sms
import matplotlib
matplotlib.use('GTK3Agg')
import matplotlib.pyplot as plt

cycles = ['1.50.5', '31', '62', '93']
codedmp = {}
iperf = {}
uncodedmp = {}

codedmp_dtype = ['f8', 'f8', 'i8', 'i8', 'i8', 'i8', 'i8']
codedmp_dtypenames = ["timestamp", "total_time", "recv_packets", "dec_packets", "throughput", "goodput", "feedbacks"]

iperf_dtype = ['U14', 'U15', 'i8', 'U15', 'i8', 'i8', 'U15', 'i8', 'i8']
iperf_dtypenames = ["timestamp", "dst_ip", "dst_port", "src_ip", "src_port", "XXX", "interval", "total_bytes",
                    "goodput"]

uncodedmp_dtype = ['f8', 'f8', 'i8', 'i8', 'i8', 'i8', 'i8']
uncodedmp_dtypenames = ["timestamp", "total_time", "recv_packets", "dec_packets", "throughput", "goodput", "feedbacks"]

for cycle in cycles:
    codedmp[cycle] = np.genfromtxt('../mptcp-tests/log6/codedmp/stats{}.csv'.format(cycle), delimiter=',', skip_header=0,
                                   skip_footer=0, dtype=codedmp_dtype,
                                   names=codedmp_dtypenames)
    iperf[cycle] = np.genfromtxt('../mptcp-tests/log6/iperf/stats{}.csv'.format(cycle), delimiter=',', skip_header=0,
                                 skip_footer=0,
                                 dtype=iperf_dtype,
                                 names=iperf_dtypenames)
    uncodedmp[cycle] = np.genfromtxt('../mptcp-tests/log6/uncodedmp/stats{}.csv'.format(cycle), delimiter=',',
                                     skip_header=0,
                                     skip_footer=0, dtype=uncodedmp_dtype,
                                     names=uncodedmp_dtypenames)

# Goodput over cycles
fig1, ax1 = plt.subplots()
ind = np.arange(len(cycles))
width = 0.24
yerr = [sms.DescrStatsW(iperf[cycle]["goodput"]).tconfint_mean()[1] - np.mean(iperf[cycle]["goodput"]) for cycle in
        cycles]
iperf_bar = ax1.bar(ind, [np.mean(iperf[cycle]["goodput"]) for cycle in cycles], width, color="r", yerr=yerr,
                    error_kw=dict(lw=3, capsize=5, capthick=3, ecolor="black"))
yerr = [sms.DescrStatsW(codedmp[cycle]["goodput"]).tconfint_mean()[1] - np.mean(codedmp[cycle]["goodput"]) for cycle in
        cycles]
codedmp_bar = ax1.bar(ind + width, [np.mean(codedmp[cycle]["goodput"]) for cycle in cycles], width, color="g", yerr=yerr,
                      error_kw=dict(lw=3, capsize=5, capthick=3, ecolor="black"))
yerr = [sms.DescrStatsW(uncodedmp[cycle]["goodput"]).tconfint_mean()[1] - np.mean(uncodedmp[cycle]["goodput"]) for cycle
        in
        cycles]
uncodedmp_bar = ax1.bar(ind + 2 * width, [np.mean(uncodedmp[cycle]["goodput"]) for cycle in cycles], width, color="b", yerr=yerr,
                        error_kw=dict(lw=3, capsize=5, capthick=3, ecolor="black"))
ax1.set_xticks(ind + width)
ax1.set_xticklabels(cycles)
ax1.legend((iperf_bar[0], codedmp_bar[0], uncodedmp_bar[0]), ('MP_TCP', 'CodedMP', 'UncodedMP'))
ax1.set_title('Goodput')

# Overhead over cycles
fig2, ax2 = plt.subplots()
ind = np.arange(len(cycles))
width = 0.35
codedmp_bar = ax2.bar(ind,
                      [np.mean(codedmp[cycle]["recv_packets"]) / np.mean(codedmp[cycle]["dec_packets"]) - 1 for cycle in
                       cycles], width, color="g")
uncodedmp_bar = ax2.bar(ind + width,
                        [np.mean(uncodedmp[cycle]["recv_packets"]) / np.mean(uncodedmp[cycle]["dec_packets"]) - 1 for
                         cycle in
                         cycles], width, color="b")
ax2.set_xticks(ind + width / 2)
ax2.set_xticklabels(cycles)
ax2.legend((codedmp_bar[0], uncodedmp_bar[0]), ('CodedMP', 'UncodedMP'))
ax2.set_title('Overhead')


# Number of feedbacks, for tcp use tcpdump -w and parse it
fig3, ax3 = plt.subplots()
ind = np.arange(len(cycles))
width = 0.35
yerr = [sms.DescrStatsW(codedmp[cycle]["feedbacks"]).tconfint_mean()[1] - np.mean(codedmp[cycle]["feedbacks"]) for cycle
        in cycles]
codedmp_bar = ax3.bar(ind,
                      [np.mean(codedmp[cycle]["feedbacks"]) for cycle in
                       cycles], width, color="g", yerr=yerr, error_kw=dict(lw=3, capsize=5, capthick=3, ecolor="black"))
yerr = [sms.DescrStatsW(uncodedmp[cycle]["feedbacks"]).tconfint_mean()[1] - np.mean(uncodedmp[cycle]["feedbacks"]) for cycle
        in cycles]
uncodedmp_bar = ax3.bar(ind + width,
                      [np.mean(uncodedmp[cycle]["feedbacks"]) for cycle in
                       cycles], width, color="b", yerr=yerr, error_kw=dict(lw=3, capsize=5, capthick=3, ecolor="black"))
ax3.set_xticks(ind + width / 2)
ax3.set_xticklabels(cycles)
ax3.legend((codedmp_bar[0], uncodedmp_bar[0]), ('CodedMP', 'UncodedMP'))
ax3.set_title('Number of feedbacks')

plt.show()

# overhead over cycles
# number feedbacks, auch tcp
# confidence interval
#
