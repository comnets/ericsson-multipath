#! /usr/bin/python3
import sys
import numpy as np
import matplotlib
import datetime as dt
import matplotlib.dates as md

matplotlib.use('GTK3Cairo')
import matplotlib.pyplot as plt

step_pos = "mid"
# dtype = [('timestamp', 'i8'), ('iface', 'U10'),
#          ('bytes_out/s', 'f8')]  # , 'f8', 'f8', 'i8', 'i8', 'f8', 'f8', 'f8', 'i8', 'i8', 'f8', 'f8', 'i8', 'i8']
dtype = ['i8', 'U10', 'f8']  # , 'f8', 'f8', 'i8', 'i8', 'f8', 'f8', 'f8', 'i8', 'i8', 'f8', 'f8', 'i8', 'i8']
names = ["timestamp", "iface_name", "bytes_out_s"]
bwm = np.genfromtxt('../mptcp-tests/old_log/log2/iperf/bwm31.csv', delimiter=';', skip_header=1, skip_footer=0,
                    dtype=dtype,
                    names=names)

dtype = ['i8', 'U10', 'i8']  # , 'f8', 'f8', 'i8', 'i8', 'f8', 'f8', 'f8', 'i8', 'i8', 'f8', 'f8', 'i8', 'i8']
names = ["timestamp", "iface_name", "bw_kbits"]
s2 = np.genfromtxt('../mptcp-tests/old_log/log2/iperf/s2-eth1_31.csv', delimiter=',', skip_header=1, skip_footer=0,
                   dtype=dtype,
                   names=names)

dtype = ['i8', 'U10', 'i8']  # , 'f8', 'f8', 'i8', 'i8', 'f8', 'f8', 'f8', 'i8', 'i8', 'f8', 'f8', 'i8', 'i8']
names = ["timestamp", "iface_name", "bw_kbits"]
s3 = np.genfromtxt('../mptcp-tests/old_log/log2/iperf/s3-eth1_31.csv', delimiter=',', skip_header=1, skip_footer=0,
                   dtype=dtype,
                   names=names)


def filter_iface(bwm, iface):
    filtered_list = []
    for counter, value in np.ndenumerate(bwm):
        if iface == value[1]:
            filtered_list.append(counter)
    return np.take(bwm, filtered_list)


def sync_timestamps(iface1, iface2):
    iface1 = iface1
    iface2 = iface2
    finished = False
    # print("Lengths: {},{}".format(len(iface1), len(iface2)))
    if len(iface2) < len(iface1):
        while True:
            old_length1 = len(iface1)
            old_length2 = len(iface2)
            for counter, value in enumerate(iface1):
                # print(counter, value)
                # print("Counter: {}".format(counter))
                # print("Lengths: {},{}".format(len(iface1), len(iface2)))
                if value[0] < iface2[counter][0] and counter == 0:
                    iface2 = np.insert(iface2, counter, (value[0], iface2[counter][1], 0))
                    # print("break 1")
                    break
                elif value[0] > iface2[counter][0] and counter == 0:
                    iface1 = np.insert(iface1, counter, (iface2[counter][0], iface1[counter][1], 0))
                    # print("break 2")
                    break
                elif value[0] < iface2[counter][0]:
                    iface2 = np.insert(iface2, counter, (value[0], iface2[counter][1], iface2[counter - 1][2]))
                    # print("break 3")
                    break
                elif value[0] > iface2[counter][0]:
                    iface1 = np.insert(iface1, counter,
                                       (iface2[counter][0], iface1[counter][1], iface1[counter - 1][2]))
                    # print("break 4")
                    break
                else:
                    # print("timestamp: {}".format(value[0]))
                    pass
                # print("Lengths1: {},{}".format(old_length1, len(iface1)))
                # print("Lengths2: {},{}".format(old_length2, len(iface2)))
            if old_length1 == len(iface1) and old_length2 == len(iface2):
                # print("finished syncing timestamps")
                break
    else:
        while True:
            old_length1 = len(iface1)
            old_length2 = len(iface2)
            for counter, value in enumerate(iface2):
                # print(counter, value)
                # print("Counter2: {}".format(counter))
                # print("Lengths2: {},{}".format(len(iface1), len(iface2)))
                if counter == len(iface1):
                    break
                if value[0] < iface1[counter][0] and counter == 0:
                    iface1 = np.insert(iface1, counter, (value[0], iface1[counter][1], 0))
                    # print("break 1")
                    break
                elif value[0] > iface1[counter][0] and counter == 0:
                    iface2 = np.insert(iface2, counter, (iface1[counter][0], iface2[counter][1], 0))
                    # print("break 2")
                    break
                elif value[0] < iface1[counter][0]:
                    iface1 = np.insert(iface1, counter, (value[0], iface1[counter][1], iface1[counter - 1][2]))
                    # print("break 3")
                    break
                elif value[0] > iface1[counter][0]:
                    iface2 = np.insert(iface2, counter,
                                       (iface1[counter][0], iface2[counter][1], iface2[counter - 1][2]))
                    # print("break 4")
                    break
                else:
                    # print("timestamp: {}".format(value[0]))
                    pass
                # print("Lengths1: {},{}".format(old_length1, len(iface1)))
                # print("Lengths2: {},{}".format(old_length2, len(iface2)))
            if old_length1 == len(iface1) and old_length2 == len(iface2):
                break
    return iface1, iface2


s2, s3 = sync_timestamps(s2, s3)
# print("finished syncing timestamps")

fig1 = plt.figure()
ax1 = fig1.add_subplot(111)
min_len = min(len(s2["bw_kbits"]), len(s3["bw_kbits"]))
# print(min(len(s2["bw_kbits"]), len(s3["bw_kbits"])), len(s2["bw_kbits"][:min_len]), len(s3["bw_kbits"][:min_len]))
timestamps = s2["timestamp"][:min_len]
dates = [dt.datetime.fromtimestamp(float(ts / 1000)) for ts in timestamps]
xfmt = md.DateFormatter('%H:%M:%S')
ax1.xaxis.set_major_formatter(xfmt)
plt.gca().xaxis.set_major_formatter(md.DateFormatter('%m/%d/%Y'))
# plt.gca().xaxis.set_major_locator(md.DayLocator())
y = np.row_stack((s2["bw_kbits"][:min_len], s3["bw_kbits"][:min_len]))
paths = ax1.stackplot(dates, s2["bw_kbits"][:min_len], s3["bw_kbits"][:min_len], step=step_pos,
                      interpolate=True, labels=["Available bandwidth per LTE", "Available bandwidth per Wifi"])

# print("1")
ax2 = ax1.twinx()
# print("2")
bwm_s2 = filter_iface(bwm, "s2-eth2")
bwm_s3 = filter_iface(bwm, "s3-eth2")
# print("s2: {}".format(bwm_s2))
# print(s2["timestamp"][-1] / 1000, s3["timestamp"][-1] / 1000, bwm_s2["timestamp"][-1][0], bwm_s3["timestamp"][-1][0])
min_len = min(len(bwm_s2["bytes_out_s"]), len(bwm_s3["bytes_out_s"]))
# print("3")
timestamps = bwm_s2["timestamp"][:min_len]
dates = [dt.datetime.fromtimestamp(float(ts[0])) for ts in timestamps]
# xfmt = md.DateFormatter('%H:%M:%S')
# ax2.xaxis.set_major_formatter(xfmt)
# plt.gca().xaxis.set_major_formatter(md.DateFormatter('%m/%d/%Y'))
# plt.gca().xaxis.set_major_locator(md.DayLocator())
# print("4")
bandwidths = (bwm_s2["bytes_out_s"][:min_len] + bwm_s3["bytes_out_s"][:min_len]) / 125
bandwidths = [elem[0] for elem in bandwidths]
# print(bandwidths)
# ax2.stackplot(x, filter_iface(bwm, "s2-eth2")["bytes_out_s"]/125, filter_iface(bwm, "s3-eth2")["bytes_out_s"]/125, step=step_pos, colors=("red", "yellow"))
throughput = ax2.step(dates, bandwidths, color="red", linewidth=2, where=step_pos, label="Actual throughput")
# print("5")
ax1.set_ylim(ax2.get_ylim())
ax1.set_ylabel("Bandwidth / Throughput in kbit/s")
ax1.set_xlabel("Time")
plt.gcf().autofmt_xdate()
legendProxies = []
path_labels = ["LTE","Wifi"]
for c, path in enumerate(paths):
    legendProxies.append(plt.Rectangle((0, 0), 1, 1, fc=path.get_facecolor()[0]))
legendProxies.append(plt.Rectangle((0, 0), 1, 1, fc=throughput[0].get_color()[0]))
plt.legend(legendProxies, ["Available bandwidth LTE", "Available bandwidth Wifi", "Actual throughput"])
plt.show()
