import threading
import queue
import socket
import sys
sys.path.append("/home/justus/Programs/kodo-python3/build/linux/src/kodo_python/")
import kodo
import struct
import helper_functions as hf
import settings as sett
import time
import random
import os
import json



class Server:
    def __init__(self):
        self.encoder_factory = kodo.FullVectorEncoderFactoryBinary8(max_symbols=sett.SYMBOLS,
                                                                    max_symbol_size=sett.SYMBOL_SIZE)

        self.file_size = None
        self.generations = None
        self.encoders = []
        self.not_finished_encoders = []
        self.generation_headers = []
        self.exitFlag = [False]
        self.data_queue = queue.Queue(sett.QUEUE_LENGTH)
        self.sent_packets = {}

        self.data_generator_thread = None
        self.feedback_thread = None
        self.send_threads = {}
        self.status_thread = None

    def prepare(self):
        s = struct.Struct('i')
        self.generations, self.file_size = hf.load_data(sett.TEST_FILE, sett.SYMBOLS, sett.SYMBOL_SIZE)
        print("Generations: {} File size: {}".format(len(self.generations), self.file_size))
        for index, generation in enumerate(self.generations):
            self.encoders.append(self.encoder_factory.build())
            self.not_finished_encoders.append(index)
            self.generation_headers.append(s.pack(index))

        for index, generation in enumerate(self.generations):
            # print("Load generation: {} Generation_len: {}".format(index, len(generation)))
            self.encoders[index].set_const_symbols(generation)

    def start_threads(self):
        self.prepare()
        self.data_generator_thread = DataGeneratorThread(self.encoders, self.not_finished_encoders,
                                                         self.generation_headers, self.data_queue, self.exitFlag)
        self.data_generator_thread.start()

        # TODO: implement weighting of interfaces
        for medium in sett.CLIENT_INF_LST:
            self.sent_packets[medium] = [0]
            self.send_threads[medium] = SendThread(sett.SERVER_INF, sett.DATA_PORT, sett.CLIENT_IP[medium],
                                                   self.data_queue,
                                                   self.sent_packets[medium], self.exitFlag)
            self.send_threads[medium].start()


        self.feedback_thread = FeedbackThread(sett.SERVER_INF, self.not_finished_encoders, self.exitFlag)
        self.feedback_thread.start()

        self.status_thread = StatusThread(self.sent_packets, self.not_finished_encoders, self.exitFlag)
        self.status_thread.start()
        # print("Data queue length: {}".format(self.data_queue.qsize()))


class DataGeneratorThread(threading.Thread):
    def __init__(self, encoders, not_finished_encoders, generation_headers, data_queue, exitFlag):
        threading.Thread.__init__(self)
        self.encoders = encoders
        self.not_finished_encoders = not_finished_encoders
        self.generation_headers = generation_headers
        self.max_header_no = len(self.generation_headers)
        self.max_header_struct = struct.Struct('i').pack(self.max_header_no)
        self.data_queue = data_queue
        self.exitFlag = exitFlag

    def run(self):
        while not self.exitFlag[0]:
            if self.data_queue.qsize() < sett.QUEUE_LENGTH and len(self.not_finished_encoders) > 0:
                # self.data_queue.put(self.encoders[random.randrange(self.max_sqe_no)].write_payload())
                generation_no = random.choice(self.not_finished_encoders)
                self.data_queue.put(
                    self.generation_headers[generation_no] + self.max_header_struct + self.encoders[
                        generation_no].write_payload())
            else:
                if self.exitFlag[0]:
                    break
                time.sleep(sett.SEND_CYCLE /10)  # TODO: implement variable for this
        print("Exit DataGeneratorThread")


class SendThread(threading.Thread):
    def __init__(self, ifname, data_port, client_ip, data_queue, sent_packets, exitFlag):
        threading.Thread.__init__(self)
        self.ifname = ifname
        self.data_port = data_port
        self.client_ip = client_ip
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        print("bind: {} with ip: {}".format(self.ifname, hf.get_ip_address(self.ifname)))
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        # self.sock.bind(("", self.data_port))
        self.data_queue = data_queue
        self.sent_packets = sent_packets
        self.exitFlag = exitFlag

    def run(self):
        while not self.exitFlag[0]:
            try:
                data = self.data_queue.get(timeout=2)
            except queue.Empty:
                break
            self.sock.sendto(data, (self.client_ip, self.data_port))
            # print("sent packet")
            self.sent_packets[0] = self.sent_packets[0] + 1
            time.sleep(sett.SEND_CYCLE) # TODO: this is problematic
        print("Exit SendThread")


class FeedbackThread(threading.Thread):
    # TODO: cleanup queue if feedback was received
    def __init__(self, ifname, not_finished_encoders, exitFlag):
        threading.Thread.__init__(self)
        self.ifname = ifname
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.bind((hf.get_ip_address(ifname), sett.FB_PORT))
        self.not_finished_encoders = not_finished_encoders
        self.exitFlag = exitFlag

    def run(self):
        while not self.exitFlag[0]:
            try:
                feedback = self.sock.recv(32 * sett.RCV_BUFFSIZE).decode("utf8")
            except socket.timeout:
                print("No feedback received")
                continue
            # print("Feedback: {}".format(feedback))
            if len(feedback) > 0:
                # print("Feedback: {}".format(feedback))
                if "finished" in feedback:
                    self.exitFlag[0] = True
                else:
                    feedback = json.loads(feedback)
                    for generation in feedback:
                        try:
                            self.not_finished_encoders.remove(int(generation))
                        except ValueError:
                            pass
            #time.sleep(sett.SEND_CYCLE / 10)
            # encoder already deleted from list TODO: should also delete actual encoder to free memory
        print("Exit FeedbackThread")


class StatusThread(threading.Thread):
    def __init__(self, sent_packets, not_finished_encoders, exitFlag):
        threading.Thread.__init__(self)
        self.sent_packets = sent_packets
        self.not_finished_encoders = not_finished_encoders
        self.sent_packets_sum = 0
        self.exitFlag = exitFlag
        self.old_time = time.time()
        self.old_data_sum = 0
        self.old_sent_packets = 0

    def run(self):
        while not self.exitFlag[0]:
            self.sent_packets_sum = 0
            for media in self.sent_packets:
                self.sent_packets_sum += self.sent_packets[media][0]
            print("Number of sent packets: {}".format(self.sent_packets))
            data_sum = self.sent_packets_sum * (8 + sett.SYMBOL_SIZE + sett.SYMBOLS + 1) * 8
            new_time = time.time()
            d_packets = self.sent_packets_sum - self.old_sent_packets
            d_sum = data_sum - self.old_data_sum
            d_time = new_time - self.old_time
            print("Throughput Mbit/s: {}".format(d_sum/1000000/d_time))
            self.old_data_sum = data_sum
            self.old_time = new_time
            self.old_sent_packets = self.sent_packets_sum
            # print("Not finished encoders: {}".format(sorted(self.not_finished_encoders)))
            if not len(self.not_finished_encoders) > 0:
                self.exitFlag[0] = True
            time.sleep(1)
        print("Exit StatusThread")


if __name__ == "__main__":
    s = Server()
    s.start_threads()
