% vim: spell spelllang=en filetype=tex
\section{Background}\label{sec:background}

\subsection{Vehicular Communication}

Vehicular communication (V2X) includes many different use cases \cite{vehicularnetworks}.
We focus on the delivery of content to a vehicle.
Standardization in this area is still in progress.
Different wireless communication technologies can be applied to this problem.
The currently widely available candidates are WiFi and LTE.
Both have already been studied in the context of vehicular networking \cite{mir2014lte,xu2017dsrc}.
Additionally, IEEE 802.11p is a recent standard for a dedicated protocol for car communication.
It focuses on ad-hoc connectivity to nearby vehicles or \acp{RSU}.

By using these radio technologies the vehicle can be connected to the Internet to allow generic content delivery.
However, different schemes have also been proposed.
Peer-to-peer distribution schemes have been proposed to decrease the delivery time \cite{nandan2005co}.
In \cite{eriksson2008cabernet} a separation between the wired portion of the delivery path and the final wireless hop is proposed to improve the performance.
We use a similar idea, but motivated by the research in \acp{MEC}, we focus only on the last hop.

In our work we aim to combine multiple radio technologies to improve the throughput of a single transmission.
We focus on LTE and WiFi as the base technologies, because they are already widely deployed and well researched.
However, our results can also be applied to include future radio technologies, such as 5G, to further improve the performance.

\subsection{Multipath}

Multipath communication offers several advantages for vehicular networks.
By utilizing multiple paths at the same time the total throughput can be increased, shortening the time to deliver the requested content.
Additionally, distributing the connection to several links allows to keep the connection alive when one link fails.
\acf{MPTCP} \cite{mptcp} is the multipath extension of TCP and currently the most widely adopted multipath transport protocol.
Several studies examined \ac{MPTCP} in the context of vehicular networks \cite{mena2017multipath,williams140828multipath}.
The results show that \ac{MPTCP} provides a persistent connection in dynamic environments.
But the throughput is often not better than the throughput of the best single path.
With asymmetric links \ac{MPTCP} can perform worse than single-path TCP.
Because \ac{MPTCP} is based on TCP, it suffers from similar issues with large \ac{BDP} and also packet loss.
Additionally, in heterogeneous networks, \ac{MPTCP} tends to underutilize the available paths\cite{BuffbloatDelMPTCPinW}.
In our work, we use \ac{MPTCP} as the baseline for the evaluation of our scheme.

\subsection{Network Coding}

Network coding was originally proposed for multicast networks \cite{AhlswedeNetFlows}.
But it has also been used as \ac{FEC} code in networks with packet loss, such as wireless networks.
TCP and \ac{MPTCP} with network coding have been proposed to improve the performance for wireless communication \cite{ctcp,tcpnc,cloud2013multi}.
Network coding is used to mask packet loss in wireless networks and thus avoids decreasing the transmission rate by the TCP congestion control.
In contrast, our work assumes no random packet loss above the data link layer.
Instead, we rely on the data link layer to ensure the delivery of packets.
This is realistic for WiFi and LTE, where the data link layer includes \ac{ARQ} mechanisms.

Network coding has also been used to improve the performance of content distribution.
\ac{CC} has been proposed as distributed content delivery mechanism \cite{chou03,nc_coop}.
In \ac{CC}, the data is split into several chunks (also called ``generations'').
Each sender uses network coding to generate coded packets from a chunk.
This scheme requires less coordination between the sending nodes.
This makes it a good fit for networks with mobile nodes and has subsequently been evaluated for vehicular networks \cite{codetorrent}.
In contrast to the previous work, which focused on peer-to-peer content distribution, we use \ac{CC} in a pure client-server transfer.
We compare \ac{CC} to TCP and MPTCP, which are the dominant protocols in this domain.
