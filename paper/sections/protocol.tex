% vim: spell spelllang=en filetype=tex
\section{Multipath Chunked Coding}\label{sec:protocol}

The protocol used in this work is an implementation of Chunked Coding\cite{chou03}.
\figurename~\ref{fig:protocol} gives an overview of the elements in the protocol.
The server reads the input file and sends coded packets to the client.
The client receives the packets, decodes the packets, and writes the result to an output file.
Successfully decoded chunks are acknowledged by sending feedback to the server.
When the file is fully acknowledged, the server and client programs terminate.

\begin{figure}
    \centering
    \includegraphics[width=\columnwidth]{figures/protocol}
    \caption{Program structure.}
    \label{fig:protocol}
\end{figure}

%\subsection{Sender}

The server splits the input file into $L$ generations (chunks).
Each generation consists of $G$ symbols of $S$ bytes.
To send a coded packet, the server randomly selects an unacknowledged generation $g$ with symbols $s = ( s_1, \ldots, s_G) $.
The server uses \ac{RLNC} on this generation to build the coded packet.
That means, a coefficient vector $\alpha = ( \alpha_1, \ldots, \alpha_G )$ with values $\alpha_i \in \mathcal{GF}(q), i=1, \ldots, G$ randomly selected from the Galois field $\mathcal{GF}(q)$ of size $q$ .
The coded payload $c$ is calculated as the linear combination $c = \alpha s^T$.
With a network code, the coefficient vector has to be sent with the coded payload.
Instead of sending all values of the coefficient vector $\alpha$, we instead send the seed $r$ of the \ac{PRNG} used to generate $\alpha$, as proposed in \cite{seedbased_coding}.
This reduces the overhead in the coded packet.
Additionally, the generation index $g$ has to be sent, so that the client can correctly decode the packet.
We use 32 bits for each the generation index $g$ and the seed $r$.
The resulting coded packet consists of the generation index $g$, the \ac{PRNG} seed $r$ and the coded payload $c$ as shown in \figurename~\ref{fig:coded_packet}.
This coded packet is then sent over UDP to an IP address of the client.
We note that $G=1$ is a special case where coding is not possible.
In this case we do not send a seed in the packet.
We evaluate this uncoded variant of the protocol, to isolate the influence of \ac{RLNC}.

\begin{figure}
    \centering
    \begin{tikzpicture}[x=0.5em, y=-1.7em]
        % invisible rectangle to center on the payload
        \path (-8, 0) rectangle (40,4);

        \draw (0,0) rectangle node {Generation Index $g$} (32,1);
        \draw (0,1) rectangle node {PRNG Seed $r$} (32,2);
        \draw (0,2) rectangle node {Coded Payload $c$ } (32,4);

        \node[anchor=east,font=\small \tt] at (0,0) {Bit 0};
        \node[anchor=east,font=\small \tt] at (0,1) {32};
        \node[anchor=east,font=\small \tt] at (0,2) {64};
        \node[anchor=east,font=\small \tt] at (0,3) {$\vdots$};
        \node[anchor=east,font=\small \tt] at (0,4) {64+$S$};
    \end{tikzpicture}
    \caption{Coded packet structure.}
    \label{fig:coded_packet}
\end{figure}

The server distributes the packets to $N$ paths with rates $R_i, i=1, \ldots, N$.
The rates $R_i$ are independent of the current path conditions.
To fully utilize each path, $R_i$ must be higher than the maximum rate supported by the corresponding path.
In our experiments, we simply send with the same rate $R$ on all paths.
Our protocol does not estimate the current conditions to plan the transmissions.
Therefore it does not consider congestion or fairness.
Because of the use of network coding, the protocol is less affected by losses caused by congestion.
In the targeted single-hop system, fairness can be achieved by \ac{AQM} in the sender and \ac{MAC} on the data link layer.
\ac{AQM} could prioritize different traffic types and thus guarantee the fairness between protocols. 
To achieve fairness on the link level the MAC layer already handles this either by an backoff for stations in the case of WiFi or the packet scheduler in the case of an LTE base station.
Thus there is no need for a redundant fairness mechanism which underestimates the available channel capacity and limits the bandwidth too much.
However, the protocol is intended for single hop edge communication and is unsuited for deep networks, where the fairness must be ensured across multiple hops.

%\subsection{Client}

The client listens for coded packets on all available IP addresses.
When a packet is received, it is assigned to the decoder of the respective generation, as specified by the packet header.
The decoder interprets the seed $r$ to regenerate the coefficient vector $\alpha$.
It stores the coefficient vector in the decoding matrix and uses Gaussian elimination to reduce the matrix.
The operations performed on the coefficient matrix are also performed on the coded payloads.
When the coefficient matrix is the identity matrix, the generation is decoded.

%\subsection{Feedback}

When a generation is fully decoded, the client will send an acknowledgement to the server.
The client will send an acknowledgement for each received packet for a decoded generation.
If packets are received after the generation is already decoded, the acknowledgement will be repeated.
This ensures that the process works fully reliable even when feedback is lost.
The acknowledgement contains only the generation's index $g$ of the decoded generation.
The client sends the feedback using UDP to the source IP and port of the previously received coded packet.
The server will mark acknowledged generations and will never use it for a coded packet again.
When all generations have been acknowledged, the server terminates.
The client program terminates when all data is decoded and it does not receive packets from the server for a specified time.

This protocol is specifically designed for file transmission in dynamic wireless channels with multi-access.
It does not use congestion control to estimate and adapt to the available bandwidth.
Instead, it sends with a constant high rate and expects lower levels to drop excessive packets.
To deal with the unfairness, data e.g. for a high precision map which has to be transmitted, can be cached inside a \ac{MEC} like described in \cite{mobile_cloud}.
It is thus not suitable for multi-hop networks.
