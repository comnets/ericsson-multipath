% vim: spell spelllang=en filetype=tex
\section{Introduction}

% Motivation
As research and development in autonomous driving and vehicular communications progress, new applications beyond video on demand and infotainment are emerging.
This includes ``Location dependent and aware'' content~\cite{vehicularnetworks}, such as 3D high-definition maps supporting precise positioning.
Data from those maps is correlated with current sensor readings, e.g. from radar or LIDAR, to precisely determine the position of vehicles \cite{autonomouscars}.
At the same time, crowd-sourcing is used to keep those maps updated as roads and environments change.
High bandwidth communication is therefore necessary to support the fast upload and frequent download of map updates.

% Problem
However, providing the best performance in wireless networks is challenging.
The properties of wireless networks are notoriously hard to predict.
The vast majority of data transmission is currently done with TCP.
But the performance of TCP suffers in unstable conditions as encountered in wireless networks\cite{tcpvar}.
New wireless technologies such as ``5G New Radio'' promise high data rates with low latency\cite{lien20175g}.
But the increase in available capacity is not easily utilized.
For example, TCP suffers from performance degradation in networks with high \ac{BDP}\cite{bdpimpactontcp}.

% Possible Solution
On the other hand, new supporting technologies emerge.
With \acp{MEC} the source of the data transfer can be moved closer to the destination.
The main goal is to reduce latency and to enhance network operation efficiency \cite{hu2015mobile}.
But it also means the communication between the vehicle and the infrastructure is not routed over the Internet.
Additionally, multipath communication can increase the available bandwidth and offers higher resilience against the failure of a single connection.

% Contribution
In this paper, we propose a protocol specifically for the use in mobile edge content delivery.
The protocol efficiently utilizes all resources of multiple paths to maximize the throughput from the server to the client.
One challenge in wireless multipath communication is the unpredictable performance.
Current approaches try to estimate the path properties and adapt to the current conditions.
In contrast, we use an agnostic approach that gives good performance without requiring accurate estimates of the channels.
This is achieved by leaving fairness and congestion control to the lower protocol layers.
Network coding is used to reduce the overhead of redundant transmissions.
We evaluate this protocol in a multipath simulation and compare it to \ac{MPTCP}.

% Structure
The paper is structured as follows.
In the following section, we will give an overview on the current state of the art of vehicular multipath data dissemination.
In Section~\ref{sec:system}, we describe the system model for our protocol, including the channel model used for the evaluation.
Section~\ref{sec:protocol} contains the full description of our proposed protocol.
An implementation of this protocol is evaluated in Section~\ref{sec:evaluation} and compared to MPTCP.
We summarize our findings in the final Section~\ref{sec:conclusion} and give an outlook for further research.
