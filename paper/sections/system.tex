% vim: spell spelllang=en filetype=tex
\section{System Model}
\label{sec:system}

In our system, we only model the last hop in the delivery process.
We assume, that our proposed solution is run at the edge of the network, close to the final last hop over a wireless channel.
In this case the capacity of the wireless transmission is the bottleneck of the connection.
We assume that the capacity in the remaining network is sufficiently available and the delay can be neglected.
This model is aimed at deployment with \acf{MEC}, where these assumptions can be fulfilled.
Further, this evaluation focuses on increasing the throughput by aggregating multiple radio technologies.
We use a topology with two nodes and two channels between them.

The main properties of communication links are throughput, delay and packet loss ratio.
All of them can be modelled by their stochastic properties including correlation in time.
They are highly coupled to each other.
For example, packet losses on the channel translate to increased delays due to \ac{ARQ} or insufficient throughput capacity causes queueing delays or even packet losses due to buffer overflow.
\ac{AMC}, as used in all modern wireless transmission systems, adds another highly complex and usually vendor depended protocol function.
More aggressive algorithms provide more throughput but at the same time increase delays due to more retransmissions triggered by packet losses due to overestimation of channel quality.

We base the parameters for our channel model on data collected by various real-world measurements and simulative studies.
Paier et al. \cite{paier2010average} measured up to 27 Mbit/s throughput for 802.11p for infrastructure to vehicle communication at short ranges and more than 3 Mbit/s even for long ranges.
The real-world measurements by Xu et al. \cite{xu2017dsrc} show throughput of up to 30 Mbit/s for LTE and 5 Mbit/s for \ac{DSRC} for a file download scenario.
This study also contains \ac{RTT} measurements of 100ms to 500ms for LTE and less than 10ms for \ac{DSRC} in different scenarios.
Another real-world measurement by Williams et al. \cite{williams140828multipath} shows throughput of up to 38 Mbit/s for 802.11n WiFi with \ac{RTT} between 10 and 40 ms.
In \cite{mir2014lte} a simulation-based study shows \ac{RTT} measurements of 20ms to 120ms for LTE.
In all measurements the performance depends heavily on the distance to the infrastructure, the velocity of the vehicle and other communicating vehicles in the are.
Thus it is clear, that the channel conditions often change over time, but not rapidly.
Informed by these measurements, we evaluate three different scenarios.

\subsection{Static Singlepath}

We evaluate the singlepath performance of our protocol with static channel properties.
In this scenario, we observe the impact of path latency and bandwidth on the performance of the protocol.
The performance is compared with TCP with various congestion control algorithms.

\subsection{Static Multipath}

In our use case for vehicular communication, a receiver can be connected using multiple radio technologies.
We evaluate a topology with two paths and measure the throughput of our proposed protocol and MPTCP.
The results of the measurements are compared with the singlepath measurements.
Ideally, a setup with several paths should achieve the throughput of the sum of the individual paths.
We measure the multipath performance and compare it with this ideal expectation.

\subsection{Dynamic Multipath}

In this scenario, we simulate two paths and modulate the properties of both paths over the runtime of a measurement.
We construct a simple two-state Markov model with different channel conditions.
In the good state, the throughput is high and latency is low.
This models a situation where the vehicle is close to the infrastructure and can send without much interference or contention.
In the bad state, the throughput is lower and the latency increases.
This emulates a contented medium, high velocity of the client or long distance to the infrastructure.
In the Markov model the time between state changes is exponentially distributed with mean values $1/\lambda$ as depicted in \figurename~\ref{fig:channel_trans}.
The state changes are independent for each path.
We chose the parameters for each state based on the previously cited studies.
The selected values are given in Table~\ref{tab:channel_states}.
The results of these measurements are compared with the static multipath measurements.
This reveals the impact of the dynamics of the channels on the performance of the protocol.
Additionally to the relative goodput, we evaluate the overhead for each protocol.

\begin{figure}
    \centering
    \begin{tikzpicture}[state/.style={draw, circle, minimum size=1.2cm}]
        \node[state] (good) at (0,0) {Good};
        \node[state] (bad)  at (4,0) {Bad};

        \draw (good) edge[bend left, -{Latex[width=3mm]}] node[anchor=south] {$\lambda_G$} (bad);
        \draw (bad) edge[bend left, -{Latex[width=3mm]}] node[anchor=north] {$\lambda_B$} (good);
    \end{tikzpicture}
    \caption{State transition diagram.}
    \label{fig:channel_trans}
\end{figure}

\begin{table}
    \caption{Multipath Configuration.}
    \centering
    \begin{tabular}{| c c | c c c c |}
        \hline
        \textbf{Link}  & \textbf{State} & \textbf{Bandwidth} & \textbf{Latency} & \textbf{Loss} & $\mathbf{1/\lambda}$ \\
        \hline
        LTE            & Good           & 100 Mbit/s         & 10ms             & 0\%           & 2s \\
                       & Bad            & 25 Mbit/s          & 100ms            & 0\%           & 1s \\
        Wifi           & Good           & 25 Mbit/s          & 10ms             & 0\%           & 2s \\
                       & Bad            & 5 Mbit/s           & 100ms            & 0\%           & 1s \\
        \hline
    \end{tabular}
    \label{tab:channel_states}
\end{table}

