% vim: spell spelllang=en filetype=tex
\section{Evaluation}
\label{sec:evaluation}

We implemented a network emulator based on Mininet \cite{mininet1} with the model described in \ref{sec:system}.
We use Linux Traffic Control (\texttt{tc})\cite{tc} to emulate the channel properties.
The throughput limitation is emulated using the Token Bucket Filter (\texttt{tbf}) module.
With the \texttt{netem}\cite{netem} module additional link delay is emulated.
Mininet does not support dynamic path properties, we therefor use a custom script to modulate the path properties.
In each experiment, we transfer a file with size 128 Mbyte and measure the runtime. Then the goodput is calculated by dividing the file size by the runtime of the transfer.
We repeat each experiment multiple times and include the 99\% confidence interval in our results.

For the baseline measurements with MPTCP, we use Linux MPTCP release 0.94.
We use this release on top of a minimal Debian Linux server installation.
We implemented a minimal sender and receiver program using TCP sockets.
The proposed multipath Chunked Coding protocol is implemented using the network coding library Kodo \cite{kodo}.
As described in Section~\ref{sec:protocol}, the rate of the sender $R$ must be chosen high enough to saturate all links.
To ensure that the sending rate does not affect the results, we aim to exceed the rate supported by the paths by several orders of magnitude.
In all experiments the rate of the sender is fixed to 1 GBit/s.

To ensure that the machine is not overloaded by the computations, we measured the actual rate of the sending program for different configurations.
The computational load depends on the field size $F$, the symbol size $S$ and the generation size $G$.
The symbol size also dictates the size of the packets sent on the network.
Therefore we choose $S=1400$ to ensure that there is enough space for the header.
In general a large field size and generation size reduces the risk of receiving redundant information.
However, it also creates a higher load on the machine.

\begin{figure}
    \includegraphics{figures/throughput}
    \caption{Sender throughput for different field sizes with increasing generation size.}
    \label{fig:sender}
\end{figure}

\figurename~\ref{fig:sender} shows the throughput achieved by the sender for different field sizes and generation sizes.
For small generation sizes the sender can keep the desired output rate of 1 Gbit/s.
When the size increases, the rate drops significantly.
For the fields $GF(2)$ the largest generation size which ensures the correct rate is 80.
For the other fields, the largest stable generation size is 32.
We use these values to evaluate the three scenarios described in section \ref{sec:system}

\subsection{Single path}

As a first measurement of the performance of our protocol, we emulate a single path network.
We evaluate the protocol under various static channel conditions to see the impact of the path parameters.
We compare the performance to single path TCP with the follwoing congestion control algorithms: CUBIC\cite{cubic}, Reno\cite{reno} and BBR\cite{bbr}.

\figurename~\ref{fig:single-gp-lat} shows the goodput achieved by the protocols.
We present the goodput relative to the capacity of the channel.
This allows us to see the difference in performance for different channel configurations.
The graph shows that an increase in link latency reduces the performance for all protocols.
TCP Cubic and TCP Reno perform close to identical.
The uncoded UDP and TCP BBR are more affected than the other protocols.
Chunked Coding with the field $GF(2^8)$ shows the best performance.

\begin{figure}
    \includegraphics{figures/sp_lat}
    \caption{Goodput relative to channel capacity for increasing link latency with fixed bandwidth 25 Mbit/s.}
    \label{fig:single-gp-lat}
\end{figure}

\figurename~\ref{fig:single-gp-bw} shows the relative goodput for varying link bandwidth.
While increasing the bandwidth improves the absolute goodput, the relative goodput declines.
As before, TCP Cubic and TCP Reno perform almost identical.
At 75 Mbit/s the performance of all TCP variants declines very quickly.
The coded protocols remain relatively stable.
The uncoded protocol declines slightly steeper than the coded protocols.
The behavior is very similar to \figurename~\ref{fig:single-gp-lat}.

In our measurements, the uncoded protocol performs better than TCP.
The TCP congestion control prevents the sender from flooding the channel, but as a result also often underestimates the capacity.
Additionally, TCP delivers all packets in order.
By lifting these restrictions, we can achieve a gain of about 2\%.
The uncoded protocol performs well for low latency and low throughput, that means a small \ac{BDP}.
For increasing \ac{BDP}, the coded protocols show a better performance.
Coding with larger field size performs better than coding with small field size.
The difference is small but consistent.
In the following evaluations we will therefor only show Chunked Coding with $GF(2^8)$.

\begin{figure}
    \includegraphics{figures/sp_bw}
    \caption{Goodput relative to channel capacity for increasing link bandwidth with fixed latency 50ms.}
    \label{fig:single-gp-bw}
\end{figure}

\subsection{Multipath}

As next step, we evaluate the protocols in different static multipath settings.
Adding a second path increases the total capacity.
In ideal conditions, the total capacity is the sum of the capacities of the path.
When both paths are combined, the total absolute goodput should correspond to the sum of the goodput for using a single path.
We compare our protocol to the Linux implementation of MPTCP with different congestion control algorithms, namely wVegas\cite{wvegas}, LIA\cite{lia}, OLIA\cite{olia} and BALIA\cite{balia}.
In our measurements we found that LIA, OLIA and BALIA perform almost identical.
We omit LIA and BALIA in the graphs, to improve the readability.
Other congestion control mechanisms for wireless networks have been proposed\cite{le2013improving,PerfEnhMPTCPinW}, but are not available in the Linux MPTCP implementation.

In \figurename~\ref{fig:mp-gp-lat} the relative goodput for a topology with two paths is shown.
One path has a bandwidth of 25Mbit/s and a latency of 50ms.
The other path has a bandwidth of 25Mbit/s and the latency is varied as shown by the x-axis.
All protocols show a steady decline with an increasing \ac{BDP}.
The decline for the uncoded UDP and MPTCP is slightly steeper than for the coded protocol.
MPTCP wVegas shows the worst performance and is much less stable than the other protocols.

\begin{figure}
    \includegraphics{figures/mp_lat}
    \caption{Goodput relative to the capacity sum of two paths with 25Mbit/s bandwidth. The latency of one path is varied while the other path remains at 50ms.}
    \label{fig:mp-gp-lat}
\end{figure}

\begin{figure}
    \includegraphics{figures/mp_bw}
    \caption{Goodput relative to the capacity sum of two paths with 50ms latency. One path has a fixed bandwidth of 50Mbit/s while the bandwidth of the other path is varied.}
    \label{fig:mp-gp-bw}
\end{figure}

\figurename~\ref{fig:mp-gp-bw} shows the relative goodput when one link has increasing bandwidth.
In this scenario, both paths have the same latency of 50ms.
One path has a fixed bandwidth of 25Mbit/s and the bandwidth of the other path is increased.
Compared to the previous figure, this shows a much stronger decline of the performance of MPTCP.
Also the performance is much less stable.

Compared to the singlepath measurements, the decline of performance for MPTCP is even worse.
MPTCP wVegas shows a high variance in performance even in this stable setting.
We therefor exclude it from the following measurements.

\subsection{Dynamic Multipath}

In our final measurements, we evaluate the performance with multiple dynamic paths.
In this scenario, the link properties change during one measurement run, as described in section~\ref{sec:system}.
The time of each state transition is recorded.
This log is used to calculate the total capacity available during the measurement by multiplying the duration of each state with the bandwidth for this state.
We compare this calculated capacity with the achieved goodput to calculate the relative goodput.
Additionally we compare the achieved goodput to the expected goodput, based on the static multipath measurements.
To calculate the expected goodput, we multiply the duration of each state with the performance measured with the same link properties in the static multipath scenario.
The gap between the expected and achieved goodput shows the ability of the protocol to handle dynamic changes of the path properties.
With the chosen parameters, each run takes about 20 seconds to complete and includes about 15 state changes.
We repeated the measurements with 20 different seeds for the path modulator.

\begin{figure}
    \includegraphics{figures/goodput_rel}
    \caption{Dynamic multipath results.}
    \label{fig:dyn-mp}
\end{figure}

\figurename~\ref{fig:dyn-mp} shows the results of our measurements.
The uncoded protocol and Chunked Coding show the best performance.
Both protocols remain very stable under the dyamic conditions.
The performance of Chunked Coding is slightly higher and almost identical to the expected performance.
The MPTCP protocols achieve less than 80\% of the available bandwidth.
Due to the high difference in behavior for the different channel states, the variance of the results is higher.
MPTCP Cubic is close to the expected goodput.
However, MPTCP OLIA performs worse than the expected goodput.
This indicates, that MPTCP OLIA does not handle the state changes well.

\subsection{Overhead}

There are three sources of overhead in our protocol: packet headers, feedback packets and redundant packets.
The packet header increases the transmitted data by a fixed number of bytes per packet.
The header of our protocol is 8 bytes.
To make it comparable to TCP we also include the 8 bytes for the UDP header to a total of 16 bytes.
With the chosen payload size of 1400 the header overhead is at about 1.1\%.
The minimal header size for TCP is 20 bytes.
In practice the header contains 10 additional bytes for the timestamps required for \ac{RTT} estimation.
\ac{MPTCP} adds an additional 20 bytes header information for control flags and a global data sequence number.
With the required padding this brings the total header size for \ac{MPTCP} to 52 bytes and thus makes up 3.5\% of each data packet.
The header overhead is added to each transferred payload.
Thus the absolute size is proportional to the file size.
The overhead relative to the goodput is thus constant.

Additionally, \ac{MPTCP} uses feedback to acknowledge the successful transmission of packets.
The overhead of the feedback packets is significant.
A TCP acknowledgement contains at least 20 bytes, but with the timestamps for \ac{RTT} estimation and the \ac{MPTCP} header it reaches a total of 40 bytes.
In the worst case, every data packet is acknowledged and thus the relative overhead can reach up to 2.8\%.
TCP supports acknowledgement aggregation to reduce this overhead.

In contrast to TCP, our protocol sends acknowledgements only when a packet is received and the corresponding generation is decoded.
Thus, an acknowledgement is sent per generation and per redundant packet.
Each acknowledgement contains only 4 bytes for the generation index.
With the 8 bytes UDP header the total size is only 12 bytes.
The relative overhead of acknowledgements depends on the generation size.
For $G=32$, as used in our experiments, the minimal overhead of acknowledgement packets is less than 0.3\%.

\begin{figure}
    \includegraphics{figures/overhead}
    \caption{Relative overhead for the dynamic multipath scenario.}
    \label{fig:overhead}
\end{figure}

The final source of overhead are redundant packets which contain information already received by the client.
Typically TCP uses SARQ, which makes sending redundant data unlikely.
However, because our protocol never stops sending until all generations are acknowledged, many redundant packets will arrive at the client.
Until the acknowledgement is received and processed, the sender has produced a full \ac{BDP} worth of data.
Parts of this data will not carry new information.
Especially at the end, when the all generations are fully decoded, the client will receive a full \ac{BDP} of redundant data.
The absolute size of this overhead is proportional to the \ac{BDP}.
This can be problematic when transmitting small file sizes in networks with high \ac{BDP}.

\figurename~\ref{fig:overhead} shows the overhead of the tested protocols in the dynamic multipath scenario.
Although the chunked coding protocol has a high overhead for redundant data, the total is almost 2\% lower than for the other protocols.
Compared to \ac{MPTCP}, this can be mainly attributed to the decrease in header size.
Additionally, compared to \ac{MPTCP} and the uncoded protocol, the overhead caused by feedback is very small.
This is especially important for half-duplex channels, where transmission of feedback interferes with the transmission of new data.

In our tested scenario the overhead caused by redundant packet transmissions for Chunked Coding is about 2.5\%.
While this is less than the 3\% shown for the uncoded protocol, this is still problematic.
As noted before, the absolute overhead is proportional to the \ac{BDP}.
For a file of half the size, this overhead would double and thus the total overhead surpasses \ac{MPTCP}.
