import os
import socket
import struct
import fcntl


# http://stackoverflow.com/questions/24196932/how-can-i-get-the-ip-address-of-eth0-in-python
def get_ip_address(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(
        s.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', ifname[:15].encode("ascii"))
    )[20:24])


def load_data(file, symbols, symbol_size):
    current_dir = os.path.dirname(__file__)
    file_path = os.path.join(current_dir, file)
    file_size = os.stat(file_path).st_size  # in bytes
    f = open(file_path, "r")
    generations = []
    generation_size = symbols * symbol_size
    while len(generations) < (file_size / generation_size) + 1:
        data = f.read(generation_size)
        if not len(data) > 0:
            break
        generations.append(data)
    f.close()
    return generations, file_size


def save_data(generations, file_size, file):
    current_dir = os.path.dirname(__file__)
    file_path = os.path.join(current_dir, file)
    try:
        os.remove(file_path)
    except:
        pass
    f = open(file_path, "a")
    for generation in range(len(generations)):
        f.write(generations[generation])
    f.truncate(file_size)
    f.close()
